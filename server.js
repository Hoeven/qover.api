const express = require('express')
const mongoose = require('mongoose')
const app = express()
const port = 4000
const router = require('./routes')

// load up express with all the goodies
const bodyParser = require('body-parser')
const compress = require('compression')
const helmet = require('helmet')
const cors = require('cors')

// parse body params, add them to req.body
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// use compression
app.use(compress())

// because I'm not going anywhere without a helmet
app.use(helmet())

// enable cors
app.use(cors())

module.exports = app

mongoose.connect('mongodb://localhost/qover', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('successfully opened connection to mongodb')
});

// mount all routes on /api path
app.use('/api', router)

app.use((err, req, res, next) => {
    return next(err)
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))