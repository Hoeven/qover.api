const mongoose = require('mongoose')

var UserSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true
  },
  password: '',
  
})

var User = mongoose.model('User', UserSchema)

module.exports = User
