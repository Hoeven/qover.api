const User = require('./user.model')

const register = async (req, res) => {
  try {
    const newUser = new User({
      email: req.body.email,
      password: req.body.password
    })

    let savedUser = await newUser.save()

    res.send(savedUser)
  } catch (e) {
    res.status(400).json('something went wrong')
  }
}

const login = async (req, res) => {
  const { email, password } = req.body
  try {
    const user = await User.findOne({ email, password })
    if (!user) res.status(401).json('access denied')
    else res.send(user)
  } catch (e) {
    res.status(400).json('something went wrong')
  }
}

module.exports = { register, login }
