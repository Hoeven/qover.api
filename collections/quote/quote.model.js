const mongoose = require('mongoose')

var QuoteSchema = new mongoose.Schema({
  priceGlobal: { type: String },
  priceUniversal: { type: String }
})

var Quote = mongoose.model('Quote', QuoteSchema)

module.exports = Quote
