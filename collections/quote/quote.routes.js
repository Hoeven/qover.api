const express = require('express')
const router = express.Router()
const controller = require('./quote.controller')

router.route('/').post(controller.getQuote)

module.exports = router
