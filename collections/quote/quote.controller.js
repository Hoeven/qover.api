// const Quote = require('./quote.model')


const calculateGlobalPrice = car => {
  switch (car) {
    case 'Audi':
      return 250
    case 'BMW':
      return 150
    case 'Porsche':
      return 500
    default: throw 'Unknown car brand entered'
  }
}

const calculateUniversalPrice = (car, price) => {
  switch (car) {
    case 'Audi':
      return 250 + (0.3 / 100 * price)
    case 'BMW':
      return 150 + (0.4 / 100 * price)
    case 'Porsche':
      return 500 + (0.7 / 100 * price)
    default: throw 'Unknown car brand entered'
  }
}

const getQuote = async (req, res) => {
  try {
    const { age, car, price } = req.body
    console.log(req.body)
    console.log('is age lower than 25 and is car porsche?')
    console.log(Number(age) < 25 && car === 'Porsche')
    if (Number(age) < 25 && car === 'Porsche') return res.status(400).json('Sorry, we do not accept this particular risk')

    const priceGlobal = calculateGlobalPrice(car)
    const priceUniversal = calculateUniversalPrice(car, Number(price))
    res.send({priceGlobal, priceUniversal})
  } catch (e) {
    console.log(e)
    res.status(400).json(e)
  }
}

module.exports = { getQuote }
