// npm imports
const express = require('express')

// custom imports
const userRoutes = require('./collections/users/user.routes')
const quoteRoutes = require('./collections/quote/quote.routes')

const router = express.Router()

// health check route to see if everything is up and running
router.get('/health-check', (req, res) => res.send('OK'))

// users route
router.use('/users', userRoutes)
// quote route
router.use('/quote', quoteRoutes)

module.exports = router
